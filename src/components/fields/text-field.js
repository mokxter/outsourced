import React from 'react'
import Formsy from 'formsy-react'
import MuiTextField from 'material-ui/TextField';

const TextField = React.createClass({
  mixins: [Formsy.Mixin],

  changeValue(event) {
    this.setValue(event.currentTarget.value);
  },

  render() {

    return <div> 
        <MuiTextField name={this.props.name} 
                  className="form-group"
                  hintText={this.props.hint}
                  onChange={this.changeValue}
                  value={this.getValue()}
                  floatingLabelText={this.props.label}
                  required={!!this.props.required}/>
      </div>

  }
})

export default TextField