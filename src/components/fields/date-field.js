import React from 'react'
import Formsy from 'formsy-react'
import DatePicker from 'material-ui/DatePicker'

const DateField = React.createClass({
  mixins: [Formsy.Mixin],

  changeValue(event, date) {
    this.setValue(date);
  },

  render() {
    return <div>
      <DatePicker
              style={this.props.style}
              onChange={this.changeValue}
              hintText={this.props.hint}
              required={!!this.props.required}/>
      {this.isPristine() || this.isValid() ? null :
        <span style={{color: "red"}}>{this.getErrorMessage()}</span>
      }
    </div>
  }
})

export default DateField