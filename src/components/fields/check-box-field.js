import React from 'react'
import Formsy from 'formsy-react'
import MuiCheckbox from 'material-ui/Checkbox';

// const styles = {
//   block: {
//     maxWidth: 250,
//   },
//   checkbox: {
//     marginBottom: 16,
//   },
// };

const TextField = React.createClass({
  mixins: [Formsy.Mixin],

  changeValue(event, isInputChecked) {
    this.setValue(isInputChecked ? true : undefined);
  },

  render() {
    return <MuiCheckbox 
                  label="I am 18 years old and older."
                  style={this.props.style}
                  onCheck={this.changeValue}
                  required={!!this.props.required}/>
  }
})

export default TextField