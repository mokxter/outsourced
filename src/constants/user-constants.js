import keyMirror from 'keymirror'

export default keyMirror({
  CREATE_USER: null
})