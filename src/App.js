import React from 'react';
import './App.css';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Paper from 'material-ui/Paper'
import RegistrationContainer from './containers/registration-container'

const App = (props) => (
  <MuiThemeProvider>
    <Paper className='form-container' zDepth={4}>
      <RegistrationContainer />
    </Paper>
  </MuiThemeProvider>
)

export default App;
