import React, { Component } from 'react'
import Formsy from 'formsy-react'

// Components
import { CardTitle, CardText } from 'material-ui/Card'
import TextField from '../components/fields/text-field'
import DateField from '../components/fields/date-field'
import CheckBoxField from '../components/fields/check-box-field'
import RaisedButton from 'material-ui/RaisedButton'

// Validations
import AgeValidator from '../utils/validations/age-validator'

// Stores and Actions
import UserStore from '../stores/user-store'
import UserActions from '../actions/user-actions'


class RegistrationContainer extends Component {
  _submit = (data) => {
    UserActions.createUser(data)
  }

  _userCallback = (data) => {
    if (UserStore.hasError) {
      alert("Error: ", UserStore.error)
    } else {
      alert("Data: ", UserStore.user)
    }
  }

  componentDidMount() {
    UserStore.addChangeListener(this._userCallback)
  }

  componentWillUnmount() {
    UserStore.removeChangeListener(this._userCallback)
  }

  render () {
    console.debug("RENDERED WEW")
    return (
      <div>
        <CardTitle title="Registration Form"/>
        <CardText>
          <Formsy.Form onSubmit={this._submit}>
            <TextField name="title"
                       hint="Mr/Ms/Mrs"
                       label="Title"
                       required/>
            <TextField name="first-name"
                       hint="John"
                       label="First Name"
                       required/>
            <TextField name="last-name"
                       hint="Doe"
                       label="Last Name"
                       required/>
            <TextField name="street"
                       hint="Acre Street"
                       label="Street"
                       required/>
            <TextField name="town"
                       hint="Denton"
                       label="Town"
                       required/>
            <TextField name="postalcode"
                       hint="M34 2AL"
                       label="Post Code"
                       required/>
            <DateField style={{marginTop: "25px"}}
                       name="birthdate"
                       hint="Birthday"
                       validations={{
                         ageValidator: AgeValidator
                       }}
                       required/>
            <CheckBoxField
                       style={{marginTop: "25px"}}
                       name="confirm18"
                       required/>
            <RaisedButton type="submit"
                          label="Submit"
                          primary={true}
                          style={{marginTop: "20px"}}
                          />
          </Formsy.Form>
        </CardText>
      </div>
    )
  }
}

export default RegistrationContainer