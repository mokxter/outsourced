import AppDispatcher from '../AppDispatcher'
import UserConstants from '../constants/user-constants'

export default {
  createUser: data => {
    AppDispatcher.handleAction({
      actionType: UserConstants.CREATE_USER,
      data: data
    })
  }
}