import AppDispatcher from '../AppDispatcher'
import EventEmitter from 'events'
import UserConstants from '../constants/user-constants'
import User from '../models/user-model'

let _hasError, _isLoading, _user, _error

class UserStore extends EventEmitter {
  get hasError() { return _hasError }
  set hasError(val) { _hasError = val }

  get error() { return _error }
  set error(val) { _error = val }

  get user() { return _user}
  set user(val) { _user = val }

  emitChange() {
    this.emit('change')
  }

  addChangeListener(callback) {
    this.on('change', callback)
  }

  removeChangeListener(callback) {
    this.removeListener('change', callback)
  }

  initialize() {
    _hasError = false
    _isLoading = false
    _user = null
    _error = null
  }
}

const userStore = new UserStore()

userStore.dispatcherIndex = AppDispatcher.register( payload => {
  const action = payload.action
  const data = action.data

  switch (action.actionType) {
    case UserConstants.CREATE_USER:
      User.createUser(data, function (error, response) {
        if (!!error) {
          userStore.hasError = true
          userStore.user = null
          userStore.error = error
        } else {
          const data = response.body
          userStore.hasError = false
          userStore.error = null
          userStore.user = data
        }
        userStore.emitChange()
      })
      break
    default:
      break
  }

})

export default userStore