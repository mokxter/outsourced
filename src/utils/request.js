var request = require('superagent')

module.exports = {
  get: (url, params = {}, callback) => {
    return request.get(url)
      .query(params)
      .end(callback)
  },

  post: (url, params = {}, callback) => {
    return request.post(url)
      .send(params)
      .end(callback)
  },

  put: (url, params = {}, callback) => {
    return request.put(url)
      .send(params)
      .end(callback)
  }
}