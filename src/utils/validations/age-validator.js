import moment from 'moment'

export default (values, value, array) => {
  return moment().diff(moment(value),'years') < 18 ? "You must be 18 and above to register." : true
}